Ce module est là pour fournir plusieurs blocs dynamiques avec des liens vers les dossiers et les derniers articles / 
RP en rapport avec le node vu.

Le module est composé de 4 fichiers : 
-- lqdn_liens.install
        Un grand classique des modules Drupal. Il est vide.

-- lqdn_liens.info
        Un grand classique des modules Drupal. Il contient le strict nécessaire. Voici la doc des .info :  
        http://drupal.org/node/171205

-- lqdn_liens.module
        C'est le fichier principal. Il utilise un hook et deux fonctions persos : 
         -- hook_block
		http://api.drupal.org/api/function/hook_block/6
               	Fourni les blocks.
		Les blocs sont définis après "case 'list':".
		Le contenu des blocs (la principale partie) est après "case 'view':".
		Il y a trois blocs : 
		-- Premier bloc : les dossiers
			Une image de chaque dossier dont ce node fait parti.
		-- Second bloc : liste actu
			La liste des actus des mêmes dossiers.
		-- Troisième bloc : liste RP
			La liste des RP des mêmes dossiers.
		Les blocs ne s'affiche (titre + contenu) que s'il y a du contenu
		Ceci est dû a la condition suivante  : if($contenu!='')"
        -- _lqdn_liens_liste
		Liste les nodes (contenu en HTML, une liste) en fonction du type (actualite ou revue_de_presse)
		et des arguments (la taxo).
	-- _lqdn_liens_verifier_presence_tag($_tags_du_node, $_tags_attendus, $_dossier, $_dossiers)
		Compare la liste des tags du node avec la liste des tags de chaque dossier.
		Si UN tag correspond, on dit que ce node appartient à ce dossier.
		Il sert donc à faire le lien entre node - dossier.

-- configuration.php
	Ce fichier sert à définir le tableau $informations.
	C'est ce fichier qu'il faut modifier lorsque on ajoute un nouveau dossier.
	Il est de la forme suivante : 
$informations = array(
	'langue1' => array(
		'dossier1' => array(
			'nom' => 'Nom du dossier',
                        'image' => 'http://lien.vers.une/image/du/dossier.jpg-ou-png-ou-autre',
                        'lien' => 'lien',
                        'args' => 'les tags (séparés par des "/") sous une forme pour les views (pas utilisé)',
                        'id_tags' => array(liste des ID des tags),
            	),
	),
);

	Si est un dossier est utilisé dans plusieurs langues, il suffit de copier le remettre dans les autres langues.
