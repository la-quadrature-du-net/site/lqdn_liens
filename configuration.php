<?php
$informations['fr'] = array(
    'paquet_telecom' => array(
        'nom' => 'Paquet Telecom',
        'image' => 'http://www.laquadrature.net/files/Telecoms_Package_fr_150px.png',
        'lien' => 'Telecoms_Package',
        'args' => 'paquet-telecom',
        'id_tags' => array(81, 82, 103, 104),
    ),
    'neutralite_du_net' => array(
        'nom' => 'Neutralité du Net',
        'image' => 'http://www.laquadrature.net/files/net_neutrality_intro_250px.jpg',
        'lien' => 'neutralite_du_Net',
        'args' => 'neutralité-du-net-|-discrimination-du-net',
        'id_tags' => array(107, 108),
    ),
    'hadopi' => array(
        'nom' => 'HADOPI',
        'image' => 'http://www.laquadrature.net/files/hadopi_150px.png',
        'lien' => 'HADOPI',
        'args' => 'loi-hadopi',
        'id_tags' => array(3, 4, 35, 37),
    ),
    'filtrage' => array(
        'nom' => 'Filtrage',
        'image' => 'http://www.laquadrature.net/files/loppsi_150px.png',
        'lien' => 'filtrage-du-net',
        'args' => 'filtrage/loi-"jeux-en-ligne"/loppsi',
        'id_tags' => array(5, 39, 111, 116),
    ),
    /*'financement_mutualise' => array(
        'nom' => 'Financement mutualisé',
        'image' => '',
        'lien' => '',
        'args' => 'financement-mutualisé',
        'id_tags' => array(94),
    ),*/
    'acta' => array(
        'nom' => 'ACTA',
        'image' => 'http://www.laquadrature.net/files/acta_150px.png',
        'lien' => 'acta',
        'id_tags' => array(118),
    ),
    'ceta' => array(
        'nom' => 'CETA',
        'image' => 'http://www.laquadrature.net/wiki/File:CETA-lqdn-250.png',
        'lien' => 'ceta',
        'id_tags' => array(158),
    ),
);
$informations['en'] = array(
    'paquet_telecom' => array(
        'nom' => 'Telecoms Package',
        'image' => 'http://www.laquadrature.net/files/Telecoms_Package_200px.png',
        'lien' => 'Telecoms_Package',
        'args' => 'telecom-package',
        'id_tags' => array(81, 82, 103, 104),
    ),
    'neutralite_du_net' => array(
        'nom' => 'Net Neutrality',
        'image' => 'http://www.laquadrature.net/files/net_neutrality_intro_250px.jpg',
        'lien' => 'Net_neutrality',
        'args' => 'Net-neutrality-|-Net-discrimination',
        'id_tags' => array(5, 39, 111, 116),
    ),
    'acta' => array(
        'nom' => 'ACTA',
        'image' => 'http://www.laquadrature.net/files/acta_150px.png',
        'lien' => 'acta',
        'id_tags' => array(118),
    ),
    'ceta' => array(
        'nom' => 'CETA',
        'image' => 'http://www.laquadrature.net/wiki/File:CETA-lqdn-250.png',
        'lien' => 'ceta',
        'id_tags' => array(158),
    ),
);
